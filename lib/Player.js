const Character = require('./Character');

class Player extends Character {
	constructor(data, sock) {
		super(data);
		this.sock = sock;
		this.flags.monster = false;
		this.flags.started = false;
		this.flags.ready = true;
		this.room = sock.state.RoomManager.startingRoom;
		this.isInFight = false;
		this.health = 100;
		this.maxHealth = 100;
		this.gold = 99;
	}

	isMonster() {
		return false;
	}

	isReady() {
		return this.flags.ready;
	}

	isStarted() {
		return this.flags.started;
	}

	isInFight() {
		return this.isInFight;
	}

	doesJoinBattle() {
		return this.flags.joinBattle;
	}

	getSocket() {
		return this.sock;
	}

	getRoom() {
		return this.room;
	}

	start() {
		this.flags.started = true;
		this.flags.ready = false;
		this.sock.state.RoomManager.getRoom(this.getRoom()).addPlayer(this);
		this.sock.handler.sendAllInfo();
	}

	takeDamage(hp) {
		this.health -= hp;
		if (this.health <= 0) {
			this.flags.alive = false;
			this.sock.sendData(this.sock.handler.makeError(0, 'You died. Game over.'));
			this.sock.endConnection();
		}
	}

	incAttack() {
		let incattack = new Buffer.alloc(86);

		incattack.writeUInt8(1, 0);
		incattack.writeUInt16LE(19, 1);
		incattack.write(this.getName(), 3, 32);
		incattack.write('SERVER', 35, 32);
		incattack.write(`Increased by 1: ATT`, 67);

		this.attack += 1;
		setTimeout(() => { this.sock.sendData(incattack); }, 100);
	}

	incDefense() {
		let incdefense = new Buffer.alloc(86);

		incdefense.writeUInt8(1, 0);
		incdefense.writeUInt16LE(19, 1);
		incdefense.write(this.getName(), 3, 32);
		incdefense.write('SERVER', 35, 32);
		incdefense.write(`Increased by 1: DEF`, 67);

		this.defense += 1;
		setTimeout(() => { this.sock.sendData(incdefense); }, 100);
	}

	incRegen() {
		let incregen = new Buffer.alloc(86);

		incregen.writeUInt8(1, 0);
		incregen.writeUInt16LE(19, 1);
		incregen.write(this.getName(), 3, 32);
		incregen.write('SERVER', 35, 32);
		incregen.write(`Increased by 1: REG`, 67);

		this.regen += 1;
		setTimeout(() => { this.sock.sendData(incregen); }, 100);
	}

	incHealth() {
		let inchealth = new Buffer.alloc(86);

		inchealth.writeUInt8(1, 0);
		inchealth.writeUInt16LE(19, 1);
		inchealth.write(this.getName(), 3, 32);
		inchealth.write('SERVER', 35, 32);
		inchealth.write(`Increased by 1: HEA`, 67);

		this.health += 1;
		this.maxHealth += 1;
		setTimeout(() => { this.sock.sendData(inchealth); }, 100);
	}


	encode() {
		let buf = new Buffer.alloc(48 + (this.description.length || 0));
        let flags = (0b10000000 * this.flags.alive) + (0b01000000 * this.flags.joinBattle) + (0b00100000 * this.flags.monster) + (0b00010000 * (this.flags.started || false)) + (0b00001000 * (this.flags.ready || false)) + (0b00000100 * this.flags.unused1) + (0b00000010 * (this.flags.doesGrapple || false)) + (0b00000001 * (this.flags.gracePeriod || false));


        buf.writeUInt8(10, 0);
        buf.write(this.name, 1, 32);
        buf.writeUInt8(flags, 33);
        buf.writeUInt16LE(this.attack, 34);
        buf.writeUInt16LE(this.defense, 36);
        buf.writeUInt16LE(this.regen, 38);
        buf.writeInt16LE(this.health, 40);
        buf.writeUInt16LE(this.gold, 42);
        buf.writeUInt16LE(this.room, 44);
        buf.writeUInt16LE(this.description.length, 46);
        buf.write(this.description, 48, this.description.length);

        return buf;
	}
}
module.exports = Player;
