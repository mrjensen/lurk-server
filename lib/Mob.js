const Character = require('./Character');

class Mob extends Character {
	constructor(data, server) {
		super(data);
		this.server = server;
		this.flags.monster = true;
		this.data = data;

		setInterval(() => { this.revive() }, this.data.timer); // Revive timer
	}

	isMonster() {
		return true;
	}

	revive() {
		if (!this.isAlive()) {
			console.log(this.getName() + ' has revided.');
			this.flags.alive = true;
			this.health = this.maxHealth;
			this.attack = this.data.attack;
			this.defense = this.data.defense;
			this.regen = this.data.regen;
			this.gold = this.data.gold;

			if (this.server.state.RoomManager.getRoom(this.room).getPlayers().length > 0) this.server.state.RoomManager.getRoom(this.room).getPlayers()[0].sock.handler.sendAllInfo();
		}
	}

	encode() {
		let buf = new Buffer.alloc(48 + (this.description.length || 0));
        let flags = (0b10000000 * this.flags.alive) + (0b01000000 * this.flags.joinBattle) + (0b00100000 * this.flags.monster) + (0b00010000 * (this.flags.started || false)) + (0b00001000 * (this.flags.ready || false)) + (0b00000100 * this.flags.unused1) + (0b00000010 * (this.flags.doesGrapple || false)) + (0b00000001 * (this.flags.gracePeriod || false));


        buf.writeUInt8(10, 0);
        buf.write(this.name, 1, 32);
        buf.writeUInt8(flags, 33);
        buf.writeUInt16LE(this.attack, 34);
        buf.writeUInt16LE(this.defense, 36);
        buf.writeUInt16LE(this.regen, 38);
        buf.writeInt16LE(this.health, 40);
        buf.writeUInt16LE(this.gold, 42);
        buf.writeUInt16LE(this.room, 44);
        buf.writeUInt16LE(this.description.length, 46);
        buf.write(this.description, 48, this.description.length);

        return buf;
	}
}
module.exports = Mob;
