class Handler {
	constructor(sock) {
		this.sock = sock;
	}

	makeError(type, data) {
		let buf;
		if (type === 0) { //Other error. Specify the error message.
			let errLen = data.length;
			buf = new Buffer.alloc(4 + errLen);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(0, 1);
			buf.writeUInt16LE(errLen, 2);
			buf.write(data, 4, errLen);
		} else if (type === 1) { // Bad room.
			buf = new Buffer.alloc(59);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(1, 1);
			buf.writeUInt16LE(55, 2);
			buf.write('You attempted to move to a room that you are unable to.', 4, 55);
		} else if (type === 2) { // Player already exists.
			buf = new Buffer.alloc(57);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(2, 1);
			buf.writeUInt16LE(53, 2);
			buf.write('You attempted to create a player that already exists.', 4, 53);
		} else if (type === 3) { // Bad monster (to loot).
			buf = new Buffer.alloc(45);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(3, 1);
			buf.writeUInt16LE(41, 2);
			buf.write('You attempted to loot an invalid creature.', 4, 41);
		} else if (type === 4) { // Stat error.
			buf = new Buffer.alloc(59);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(4, 1);
			buf.writeUInt16LE(55, 2);
			buf.write('You attempted to create a character with invalid stats.', 4, 55);
		} else if (type === 5) { // Not ready.
			buf = new Buffer.alloc(52);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(5, 1);
			buf.writeUInt16LE(48, 2);
			buf.write('You attempted to do something prior to starting.', 4, 48);
		} else if (type === 6) { // No target.
			buf = new Buffer.alloc(62);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(6, 1);
			buf.writeUInt16LE(58, 2);
			buf.write('You attempted to target something that isn\'t in your room.', 4, 58);
		} else if (type === 7) { // No fight.
			buf = new Buffer.alloc(59);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(7, 1);
			buf.writeUInt16LE(55, 2);
			buf.write('You attempted to fight but there is no fight to be had.', 4, 55);
		} else if (type === 8) { // No PvP allowed.
			buf = new Buffer.alloc(56);

			buf.writeUInt8(7, 0);
			buf.writeUInt8(8, 1);
			buf.writeUInt16LE(52, 2);
			buf.write('You attempted to fight a player but PvP is disabled.', 4, 52);
		}

		return buf;
	}

	handle(type, data) { //Gets an integer type and buffer data
		return new Promise((res, rej) => {
			if (type === 1) res(this.handleMessage(data));
			else if (type === 2) res(this.handleChangeRoom(data));
			else if (type === 3) res(this.handleFight(data));
			else if (type === 4) res(this.handlePvPFight(data));
			else if (type === 5) res(this.handleLoot(data));
			else if (type === 6) res(this.handleStart(data));
			else if (type === 10) res(this.handleCharacter(data));
			else if (type === 12) res(this.handleLeave(data));
			else if (type === 100) res(this.handleOutputPlayer(data));
			else res({error: true});
		})

	}

	handleChangeRoom(data) {
		return new Promise((res, rej) => {
			let end = false;
			if (!this.sock.getPlayer() || !this.sock.getPlayer().isStarted()) {
				this.sock.sendData(this.makeError(5));
				res({end: 2});
				end = true;
			}
			if (!end) {
				let newRoom = data.readUInt16LE(0);
				let currentRoom = this.sock.state.RoomManager.getRoom(this.sock.getPlayer().getRoom());

				try {
					if (this.sock.getPlayer().isInFight) {
						this.sock.sendData(this.makeError(0, 'You can\'t run away while in a fight.'));
						res({end: 2});
					}
					let found = false;
					currentRoom.connections.forEach(conn => {
						if (conn === newRoom) {
							currentRoom.removePlayer(this.sock.getPlayer());
							this.sock.state.RoomManager.getRoom(newRoom).addPlayer(this.sock.getPlayer());
							this.sock.getPlayer().room = newRoom;
							this.sendAllInfo();
							this.sendAccept(2);
							res({end: 2});
							found = true;
						}
					});
					if (!found) {
						this.sock.sendData(this.makeError(1));
						res({end: 2});
					}
				} catch (e) {
					this.sock.sendData(this.makeError(1));
					res({end: 2});
				}
			}
		});
	}

	handleMessage(data) {
		let msgLen = data.readUInt16LE(0);
		if (!this.sock.getPlayer() || !this.sock.getPlayer().isStarted()) {
			this.sock.sendData(this.makeError(5));
			return {end: 66 + msgLen};
		}
		if (data.length < 66 + msgLen) return {wait: true};

		let decoded = {
			type: 1,
			end: 66 + msgLen,
			msgLen: msgLen,
			recpName: data.toString('utf8', 2, 32),
			sendName: data.toString('utf8', 34, 32),
			msg: data.toString('utf8', 66, 66 + msgLen)
		};

		let match = false;
		this.sock.state.RoomManager.getRoom(this.sock.getPlayer().getRoom()).getPlayers().forEach(player => {
			if (player.getName().toUpperCase() == decoded.recpName.replace(/\0/g, '').toUpperCase()) {

				let buf = new Buffer.alloc(67 + msgLen);

				buf.writeUInt8(1, 0);
		        buf.writeUInt16LE(decoded.msgLen, 1);
		        buf.write(decoded.recpName, 3, 32);
		        buf.write(this.sock.player.getName(), 35, 32);
		        buf.write(decoded.msg, 67, decoded.msgLen);

				player.sock.sendData(buf);
				this.sendAccept(1);
				match = true;
			} else return;
		});
		if (match) return {end: decoded.end};
		this.sock.sendData(this.makeError(0, 'That player is not in your room.'));
		return {end: decoded.end};
	}

	handleFight(data) {
		let end = false;
		if (!this.sock.getPlayer() || !this.sock.getPlayer().isStarted()) {
			this.sock.sendData(this.makeError(5));
			return {end: 0};
			end = true;
		} else if (this.sock.state.RoomManager.getRoom(this.sock.getPlayer().getRoom()).isAlone()) {
			this.sock.sendData(this.makeError(7));
			return {end: 0};
			end = true;
		}
		if (!end) {
			this.sendAccept(3);
			this.sock.state.FightHandler.doFight(this.sock.player);
			return {end: 0};
		}
	}

	handlePvPFight(data) {
		this.sock.sendData(this.makeError(8));
		return {end: 32};
	}

	handleLoot(data) {
		this.sock.sendData(this.makeError(0, 'This server has looting disabled. When you kill a creature you automatically get its gold.'));
		return {end: 32};
	}

	handleStart(data) {
		if (!this.sock.getPlayer()) {
			this.sock.sendData(this.makeError(0, 'You need a character before starting.'));
			return {end: 0};
		}
		if (this.sock.getPlayer().isReady()) {
			this.sock.getPlayer().start();
			this.sendAccept(6);

			this.sock.server.clients.forEach(client => {
				let joinMsg = new Buffer.alloc(88 + this.sock.player.getName().length);

				joinMsg.writeUInt8(1, 0);
		        joinMsg.writeUInt16LE(21 + this.sock.player.getName().length, 1);
		        joinMsg.write(this.sock.player.getName(), 3, 32);
		        joinMsg.write('SERVER', 35, 32);
		        joinMsg.write(`${this.sock.player.getName()} has join the server.`, 67, 87 + this.sock.player.getName().length);

				client.sendData(joinMsg);
			});
		} else {
			this.sock.sendData(this.makeError(0, 'You attempted to start when you are not yet ready.'));
		}
		return {end: 0};
	}

	handleCharacter(data) {
		let descLen = data.length >= 46 ? data.readUInt16LE(45) : 0;
		if (data.length < 47 + descLen) return {wait: true};

		let char = {
			type: 10,
			end: 47 + descLen,
			name: data.toString('utf8', 0, 32).replace(/\0/g, ''),
			flags: {
				joinBattle: data.readUInt8(32) & 0b01000000 ? true : false,
			},
			attack: data.readUInt16LE(33),
			defense: data.readUInt16LE(35),
			regen: data.readUInt16LE(37),
			descLen: descLen,
			description: data.toString('utf8', 47, 47 + descLen)
		};

		if (this.sock.hasPlayer()) {
			this.sock.sendData(this.makeError(0, 'This server doesn\'t allow changing your character.'));
			return {end: 47 + descLen};
		}
		//Validate if the character is allowed
		if (char.attack + char.defense + char.regen > process.env.INITIAL_POINTS) {
			this.sock.sendData(this.makeError(4))
			return {end: 47 + descLen};
		}

		let taken = false;
		this.sock.server.clients.forEach(client => {
			if (client.hasPlayer() && client.player.getName().toUpperCase() == char.name.toUpperCase()) {
				this.sock.sendData(this.makeError(0, 'The name you chose is already taken.'));
				taken = true;
			}
		});
		if (taken) return {end: 47 + descLen};

		this.sock.setPlayer(char);
		this.sendAccept(10);
		return {end: 47 + descLen};
	}

	handleLeave(data) {
		this.sendAccept(12);
		this.sock.endConnection();
		return {end: 0};
	}

	sendJoinData() {
		let buf = new Buffer.alloc(7 + process.env.GAME_DESCRIPTION.length);

        buf.writeUInt8(11);
        buf.writeUInt16LE(process.env.INITIAL_POINTS, 1);
        buf.writeUInt16LE(process.env.STAT_LIMIT, 3);
        buf.writeUInt16LE(process.env.GAME_DESCRIPTION.length, 5);
        buf.write(process.env.GAME_DESCRIPTION, 7, process.env.GAME_DESCRIPTION.length);

		this.sock.sendData(buf);
	}

	sendAllInfo() {
		let room = this.sock.state.RoomManager.getRoom(this.sock.getPlayer().getRoom());
		room.players.forEach(p => {
			this.sock.sendData(p.sock.getPlayer().encode());
			p.sock.sendData(room.encode());
			room.players.forEach(player => {
				if (player !== p.sock.player) p.sock.sendData(player.encode());
			});
			room.mobs.forEach(mob => {
				p.sock.sendData(mob.encode());
			});
			room.connections.forEach(conn => {
				p.sock.sendData(p.sock.state.RoomManager.getRoom(conn).encodeConn())
			});
		})
	}

	sendAccept(type) {
		let buf = new Buffer.alloc(2);

		buf.writeUInt8(8, 0);
		buf.writeUInt8(type, 1);

		this.sock.sendData(buf);
	}












	//-----DEBUG-----//
	handleOutputPlayer(data) {
		this.sock.sendData(this.sock.player.encode());
		return {end: 0};
	}
}
module.exports = Handler;
