class Room {
	constructor(data) {
		this.id = data.id;
		this.name = data.name;
		this.description = data.description;
		this.connections = data.connections;

		this.mobs = new Set();
		this.players = new Set();
	}

	isAlone() {
		if (this.players.length === 1 && this.mobs.length === 0) return true;
		return false;
	}

	addPlayer(player) {
		this.players.add(player);
	}

	removePlayer(player) {
		this.players.delete(player);
	}

	addMob(mob) {
		this.mobs.add(mob);
	}

	removeMob(mob) {
		this.mobs.delete(mob);
	}

	spawnMob(state, id) {
		state.MobFactory.create(id, this.id).then(mob => {
			this.addMob(mob)
		}).catch(e => {});
	}

	getPlayers() {
      return Array.from(this.players.values());
    }

	encode() {
		let buf = new Buffer.alloc(37 + this.description.length);

		buf.writeUInt8(9, 0);
		buf.writeUInt16LE(this.id, 1);
		buf.write(this.name, 3, 32);
		buf.writeUInt16LE(this.description.length, 35);
		buf.write(this.description, 37, this.description.length);

		return buf;
	}

	encodeConn() {
		let buf = new Buffer.alloc(37 + this.description.length);

		buf.writeUInt8(13, 0);
		buf.writeUInt16LE(this.id, 1);
		buf.write(this.name, 3, 32);
		buf.writeUInt16LE(this.description.length, 35);
		buf.write(this.description, 37, this.description.length);

		return buf;
	}
}
module.exports = Room;
