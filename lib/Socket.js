const Handler = require('./Handler');
const Player = require('./Player');

class Socket {
	constructor(sock, server) {
		this.server = server;
		this.state = server.state;
		this.sock = sock;
		this.player;
		this.buffer = new Buffer.alloc(0);
		this.handler = new Handler(this);

		this.handler.sendJoinData();

		this.sock.on('data', data => {
			this.appendBuffer(new Buffer.from(data));
			this.checkBuffer();
		});
		this.sock.on('close', err => {
			server.removeClient(this);
			this.state.RoomManager.getRoom(this.player.room).removePlayer(this.player);
		});
	}

	hasPlayer() {
		return (this.player ? true : false);
	}

	getPlayer() {
		return this.player;
	}

	getSocket() {
		return this.sock;
	}

	getBuffer() {
		return this.buffer;
	}

	setPlayer(player) {
		this.player = new Player(player, this);
	}

	clearBuffer() {
		this.buffer = new Buffer.alloc(0);
	}

	appendBuffer(buf) {
		this.buffer = new Buffer.concat([this.buffer, buf]);
	}

	sliceBuffer(n) {
		this.buffer = this.buffer.slice(n + 1);
		this.checkBuffer();
	}

	checkBuffer() {
		if (this.getBuffer().length <= 0) return; //Nothing to check.
		let type  = this.getBuffer().readUInt8(0); //First item in buffer. Type sent.
		this.handler.handle(type, this.getBuffer().slice(1)).then(handled => {
			if (handled.wait) return;
			if (handled.error) {
				this.sendData(this.handler.makeError(0, 'The server tried to read invalid data and has cleared all data.'));
				this.clearBuffer();
				return;
			}

			this.sliceBuffer(handled.end);
		}).catch(e => {});
	}

	sendData(data) {
		this.sock.write(data);
	}

	endConnection() {
		this.sock.destroy();
	}
}
module.exports = Socket;
