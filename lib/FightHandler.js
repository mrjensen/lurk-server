const RandomUtil = new (require('./RandomUtil'))();

class FightHandler {
	constructor(server) {
		this.server = server;
	}

	doFight(player) {
		let fighters = [player];
		let mobs = [];
		let players = [player];

		this.server.state.RoomManager.getRoom(player.room).mobs.forEach(mob => {
			if (mob.health > 0) {
				mobs.push(mob);
				fighters.push(mob);
			}
		});
		this.server.state.RoomManager.getRoom(player.room).players.forEach(p => {
			if (p !== player && p.doesJoinBattle()) {
				players.push(p);
				fighters.push(p);
			}
		});

		this.rollInitiative(fighters).then(sorted => {
			fighters = sorted;
			while(mobs.length > 0 && players.length > 0) {
				let newLists = this.fightRound(fighters, players, mobs);
				fighters = newLists[0];
				players = newLists[1];
				mobs = newLists[2];
			}
			try {
				player.sock.state.RoomManager.getRoom(player.room).getPlayers()[0].sock.handler.sendAllInfo();
			} catch (e) {
				console.error(e)
			}
		}).catch(err => { console.error(err)});
	}

	rollInitiative(list) {
		return new Promise((res, rej) => {
			let order = [];
			let sorted = [];

			list.forEach(fighter => {
				order.push([fighter, RandomUtil.randRangeInc(1, 100)]);
			});
			order.sort((a, b) => { return a[1] - b[1]; });
			order.forEach(fighter => {
				sorted.push(fighter[0]);
			});
			res(sorted);
		});
	}

	fightRound(f, p, m) {
		let newF = [];
		f.forEach(fighter => {
			if (fighter.health <= 0 || p.length === 0 || m.length === 0) return;
			let defender;
			let type;
			if (p.indexOf(fighter) > -1) {
				defender = RandomUtil.randItem(m);
				type = 'm';
			} else {
				defender = RandomUtil.randItem(p);
				type = 'p';
			}

			let str = RandomUtil.randRoll(fighter.attack + 1);
			let def = RandomUtil.randRoll(defender.defense + 1);

			if (str === def && RandomUtil.randBool) {
				defender.takeDamage(1);
			} else if (str > def) {
				defender.takeDamage(str - def);
			} else {
			}

			if (!defender.isAlive()) {
				fighter.addGold(defender.takeGold());
				while (!fighter.isMonster() && fighter.gold >= 100) {
					fighter.useGold();
					let stat = RandomUtil.randItem(['a', 'd', 'r', 'h']);

					if (stat == 'a') fighter.incAttack();
					else if (stat == 'd') fighter.incDefense();
					else if (stat == 'r') fighter.incRegen();
					else if (stat == 'h') fighter.incHealth();
				}
				if (type == 'p') p.splice(p.indexOf(defender), 1);
				else m.splice(m.indexOf(defender), 1);

			}

			if (RandomUtil.randRangeInc(1, 4) == 4) {
				fighter.doRegen();
			}
		});

		f.forEach(fighter => {
			if (fighter.isAlive()) newF.push(fighter);
		});

		return [newF, p, m];
	}

}
module.exports = FightHandler;
