const Room = require('./Room');

class RoomManager {
	constructor() {
		this.rooms = new Map();
		this.startingRoom = null;
	}

	getRoom(id) {
		return this.rooms.get(id);
	}

	addRoom(room) {
		this.rooms.set(room.id, room);
	}

	removeRoom(room) {
		this.rooms.delete(room.id);
	}
}
module.exports = RoomManager;
