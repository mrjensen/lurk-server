class Character {
	constructor(data, state) {
		this.state = state;
		this.name = data.name;
		this.attack = data.attack;
		this.defense = data.defense;
		this.regen = data.regen;
		this.health = data.health;
		this.maxHealth = data.maxHealth || 100;
		this.flags = {
			alive: true,
			joinBattle: data.flags.joinBattle,
		};
		this.gold = data.gold;
		this.room = data.room;
		this.description = data.description;
	}

	isAlive() {
		return this.flags.alive;
	}

	getName() {
		return this.name;
	}

	doRegen() {
		if (this.health + this.regen > this.maxHealth) this.health = this.maxHealth;
		else this.health += this.regen;
	}

	takeDamage(hp) {
		this.health -= hp;
		if (this.health <= 0) {
			this.flags.alive = false;
		}
	}

	addGold(gp) {
		this.gold += gp;
	}

	takeGold() {
		let gold = this.gold;
		this.gold = 0;
		return gold;
	}

	useGold() {
		this.gold -= 100;
	}

	incAttack() {
		this.attack += 1;
	}

	incDefense() {
		this.defense += 1;
	}

	incRegen() {
		this.regen += 1;
	}

	incHealth() {
		this.health += 1;
		this.maxHealth += 1;
	}
}
module.exports = Character;
