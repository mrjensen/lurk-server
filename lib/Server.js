const fs = require('fs');
const net = require('net');

const Room = require('./Room');
const Socket = require('./Socket');

class Server {
	constructor() {
		this.state = {
			RoomManager: new (require('./RoomManager'))(),
			MobFactory: new (require('./MobFactory'))(this),
			FightHandler: new (require('./FightHandler'))(this)
		};
		this.clients = new Set();

		this.loadRooms();
		this.listen();
	}

	loadRooms() {
		let file = fs.readFileSync(__dirname + `/../maps/${process.env.MAP_FILE}.json`);
		let roomList = JSON.parse(file).rooms;

		roomList.forEach(room => {
			let newRoom = new Room(room);
			room.mobs.forEach(mob => {
				for (let i = 0; i < mob.count; i++)	newRoom.spawnMob(this.state, mob, room.id);
			});
			this.state.RoomManager.addRoom(newRoom);
		});
		this.state.RoomManager.startingRoom = JSON.parse(file).map.starting_id;
		console.info(`Starting room: ${this.state.RoomManager.startingRoom}`);
	}

	listen() {
		net.createServer(sock => {
			this.clients.add(new Socket(sock, this));
		}).listen(process.env.PORT);
		console.info(`Server listenting on port ${process.env.PORT}`);
	}


	getClients() {
		return Array.from(this.clients.values());
	}

	removeClient(sock) {
		this.clients.delete(sock);
	}
}
module.exports = Server;
