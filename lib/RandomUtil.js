class RandomUtil {
	constructor() {
	}

	randRangeInc(min, max) {
		return parseInt(Math.random() * (max - min + 1) + min);
	}

	randRangeEx(min, max) {
		return parseInt(Math.random() * (max - min) + min);
	}

	randBool() {
		return Math.random() >= 0.5;
	}

	randRoll(max) {
		return parseInt(Math.random() * (max - 1) + 1);
	}

	randItem(list) {
		return list[Math.floor(Math.random() * list.length)]
	}
}
module.exports = RandomUtil;
