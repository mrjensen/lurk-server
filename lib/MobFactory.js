const Mob = require('./Mob');
const RandomUtil = new (require('./RandomUtil'))();

const fs = require('fs');

class MobFactory {
	constructor(server) {
		this.server = server;
		let file = fs.readFileSync(__dirname + `/../maps/${process.env.MAP_FILE}.json`);
		this.mobList = JSON.parse(file).mobs;
	}

	create(id, room) {
		return new Promise((res, rej) => {
			this.mobList.forEach(mob => {
				if (mob.id === id.id) {
					let health = RandomUtil.randItem(mob.health);
					res(new Mob({
						name: RandomUtil.randItem(mob.name),
						attack: RandomUtil.randItem(mob.attack),
						defense: RandomUtil.randItem(mob.defense),
						regen: RandomUtil.randItem(mob.regen),
						health: health,
						maxHealth: health,
						gold: RandomUtil.randItem(mob.gold),
						description: RandomUtil.randItem(mob.description),
						flags: {
							joinBattle: true,
							monster: true
						},
						room: room,
						timer: RandomUtil.randItem(mob.timer) * 1000
					}, this.server));
				}
			});
		});
	}
}
module.exports = MobFactory;
