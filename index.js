require('dotenv').config(); // Load .env file

if (process.argv[2]) {
	process.env.PORT = parseInt(process.argv[2]);
}

let map = new Map();

const Server = new (require('./lib/Server'))();

process.on('uncaughtException', (err) => {
  console.error('Error: ' + err);
});
